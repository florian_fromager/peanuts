"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.id = exports.allSet = exports.alertMsg = exports.rewriteName = exports.getParam = void 0;

// HTML PARAMETERS
var getParam = function getParam(parameterName) {
  var result = null,
      tmp = [];
  var items = location.search.substr(1).split("&");

  for (var index = 0; index < items.length; index++) {
    tmp = items[index].split("=");
    if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
  }

  return result;
};

exports.getParam = getParam;

var allSet = function allSet(queryParams, set, value) {
  queryParams.set(set, value);
  localStorage.setItem(set, value);
}; // SIMPLE FUNCTION


exports.allSet = allSet;

var rewriteCaps = function rewriteCaps(name, factor) {
  var names = name.split(factor);

  if (names.length > 0) {
    name = "";

    for (var i = 0; i < names.length; i++) {
      var part = names[i];
      part = part.charAt(0).toUpperCase() + part.slice(1);
      name += i > 0 ? "".concat(factor).concat(part) : part;
    }
  }

  return name;
};

var rewriteName = function rewriteName(name) {
  name = name.toLowerCase();
  name = rewriteCaps(name, " ");
  name = rewriteCaps(name, "-");
  return name;
};

exports.rewriteName = rewriteName;

var alertMsg = function alertMsg(alert, data) {
  alert.value = data;
  setTimeout(function () {
    alert.value = undefined;
  }, 2000);
};

exports.alertMsg = alertMsg;

var id = function id() {
  var date = Date.now().toString();
  return date.substring(8, date.length - 2);
};

exports.id = id;