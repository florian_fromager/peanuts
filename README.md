# PEANUTS

[🌐 Demo](https://peanuts-secret-santa.netlify.app)

## What is PEANUTS? 🤔

It is a simple JSON to URL secret santa app made in Vue.js. You just have to enter all the name off the draw (you can select name to avoid for each players) and begin the party. As soon as your draw is done, you just have to copy/paste the link to the next player named.

!!! Sometimes the last player only have himself to draw. WIP

## TODO

* Add price limit
* Set responsive design
* Set rules to end the game without errors